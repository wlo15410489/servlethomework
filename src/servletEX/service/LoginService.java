package servletEX.service;

import servletEX.entity.LoginUser;

public interface LoginService {
	
	boolean login(LoginUser request) ;
	
}
