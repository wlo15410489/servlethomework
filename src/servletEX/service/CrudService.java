package servletEX.service;

import java.util.List;
import java.util.Map;

import servletEX.vo.CrudVo;

public interface CrudService {

	List<Map<String, String>> read(CrudVo request);
	
	void delete(CrudVo request);
	
	void insert(CrudVo request);
	
	void update(CrudVo request);
}
