package servletEX.service.impl;

import servletEX.dao.LoginUserDao;
import servletEX.entity.LoginUser;
import servletEX.service.LoginService;

public class LoginServiceImpl implements LoginService{
	
	public boolean login(LoginUser input) {
		LoginUserDao loginUserDao = new LoginUserDao();
		boolean valid = false;
		try {
			valid = loginUserDao.checkUser(input);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return valid;
	}

}
