package servletEX.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import servletEX.dao.CrudDao;
import servletEX.entity.LoginUser;
import servletEX.service.CrudService;
import servletEX.vo.CrudVo;

public class CrudServiceImpl implements CrudService{
	
	public List<Map<String, String>> read(CrudVo input) {
		CrudDao crudDao = new CrudDao();
		LoginUser login = new LoginUser();
		List<Map<String, String>> data =new ArrayList<Map<String, String>>();
		try {
			login.setUserAccount(input.getUserAccount());
			data = crudDao.queryUser(login);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	public void delete(CrudVo input) {
		CrudDao crudDao = new CrudDao();
		LoginUser login = new LoginUser();
		try {
			login.setUserAccount(input.getUserAccount());
			login.setUserPassword(input.getUserPassword());
			crudDao.deleteUser(login);;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insert(CrudVo input) {
		CrudDao crudDao = new CrudDao();
		LoginUser login = new LoginUser();
		try {
			login.setUserAccount(input.getUserAccount());
			login.setUserName(input.getUserName());
			login.setUserPassword(input.getUserPassword());
			login.setUserPhone(input.getUserPhone());
			login.setUserSex(input.getUserSex());
			crudDao.insertUser(login);;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void update(CrudVo input) {
		CrudDao crudDao = new CrudDao();
		LoginUser login = new LoginUser();
		try {
			login.setUserId(input.getUserId());
			login.setUserAccount(input.getUserAccount());
			login.setUserPassword(input.getUserPassword());
			login.setUserName(input.getUserName());
			login.setUserPhone(input.getUserPhone());
			login.setUserSex(input.getUserSex());
			crudDao.updateUser(login);;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
