package servletEX.service;

import servletEX.entity.LoginUser;

public interface UploadService {
	
	void uploadFile(LoginUser request) ;
	
}
