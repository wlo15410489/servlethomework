package servletEX.dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class ConnectionUtils {
	public static Connection getConnection() throws Exception{
		Connection con = null;
		Class.forName("com.ibm.db2.jcc.DB2Driver");
		String url = "jdbc:db2://0.0.0.0:50000/sample";
		Properties prop = new Properties();
		prop.setProperty("user","DB2INST1");
		prop.setProperty("password","P@ssw0rd!"); 
		con = DriverManager.getConnection(url,prop); 
		
		return con;
	}
}
