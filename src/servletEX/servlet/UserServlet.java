package servletEX.servlet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servletEX.entity.LoginUser;
import servletEX.service.CrudService;
import servletEX.service.impl.CrudServiceImpl;
import servletEX.vo.CrudVo;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(value = {"/query","/userEdit","/deleteConfirm","/insertConfirm","/updateConfirm"})
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getRequestURI().substring(request.getContextPath().length());
		
		if("/query".equals(url)) {
			request.setAttribute("userList", doQuery(request, response));
			request.getRequestDispatcher("/WEB-INF/indexQuery.jsp").forward(request, response);
		}else if("/userEdit".equals(url)) {
			request.getRequestDispatcher("/WEB-INF/userEdit.jsp").forward(request, response);
		}else if("/deleteConfirm".equals(url)) {
			doDelete(request, response);
			request.getRequestDispatcher("/WEB-INF/userEdit.jsp").forward(request, response);
		}else if("/insertConfirm".equals(url)) {
			doInsert(request, response);
			request.getRequestDispatcher("/WEB-INF/userEdit.jsp").forward(request, response);
		}else if("/updateConfirm".equals(url)) {
			System.out.println("UPDATE");
			doUpdate(request, response);
			request.getRequestDispatcher("/WEB-INF/indexDelete.jsp").forward(request, response);
		}
		
	}
    
    protected List<Map<String, String>> doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	CrudService crudService = new CrudServiceImpl();
    	CrudVo crudVo = new CrudVo();
    	List<Map<String, String>> data = new ArrayList<Map<String, String>>();
    	crudVo.setUserAccount(request.getParameter("userAccount"));
    	data = crudService.read(crudVo);
    	
    	return data;
    	}
    
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	CrudService crudService = new CrudServiceImpl();
    	CrudVo crudVo = new CrudVo();
    	crudVo.setUserAccount(request.getParameter("userAccount"));
    	crudVo.setUserPassword(request.getParameter("userPassword"));
    	crudService.delete(crudVo);;
    	
    	}
    
    protected void doInsert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	CrudService crudService = new CrudServiceImpl();
    	CrudVo crudVo = new CrudVo();
    	crudVo.setUserAccount(request.getParameter("userAccount"));
    	crudVo.setUserPassword(request.getParameter("userPassword"));
    	crudVo.setUserName(request.getParameter("userName"));
    	crudVo.setUserSex(request.getParameter("userSex"));
    	crudVo.setUserPhone(request.getParameter("userPhone"));
    	crudService.insert(crudVo);;
    	
    	}
    
    protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	CrudService crudService = new CrudServiceImpl();
    	CrudVo crudVo = new CrudVo();
    	crudVo.setUserId(request.getParameter("userId"));
    	crudVo.setUserAccount(request.getParameter("userAccount"));
    	crudVo.setUserPassword(request.getParameter("userPassword"));
    	crudVo.setUserName(request.getParameter("userName"));
    	crudVo.setUserSex(request.getParameter("userSex"));
    	crudVo.setUserPhone(request.getParameter("userPhone"));
    	
    	crudService.update(crudVo);
    	}
}
