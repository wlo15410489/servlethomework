package servletEX.servlet;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import servletEX.entity.LoginUser;
import servletEX.service.CrudService;
import servletEX.service.impl.CrudServiceImpl;


/**
 * Servlet implementation class LoginServlet
 */
@MultipartConfig
@WebServlet(value = {"/upload", "/uploadPic", "/uploadFile","/uploadPicConfirm","/uploadFileConfirm"})
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getRequestURI().substring(request.getContextPath().length());
		
		if("/uploadFile".equals(url)) {
			request.getRequestDispatcher("/WEB-INF/uploadFile.jsp").forward(request, response);
		}else if("/uploadFileConfirm".equals(url)) {
			request.setAttribute("fileData", doUploadFileJsp(request, response));
			request.getRequestDispatcher("/WEB-INF/uploadFile.jsp").forward(request, response);
		}else if("/uploadPic".equals(url)) {
			request.getRequestDispatcher("/WEB-INF/uploadPic.jsp").forward(request, response);
		}else if("/uploadPicConfirm".equals(url)) {
			request.setAttribute("dataPic", doUploadPic(request, response));
			request.getRequestDispatcher("/WEB-INF/uploadPic.jsp").forward(request, response);
		}
		
	}
    
    //圖檔轉換base64
    protected String doUploadPic(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		InputStream in = null;
    		String base64 = null;
            Part file = request.getPart("picpath");
            try {
            	in = file.getInputStream();
                byte[] bytes = IOUtils.toByteArray(in);
                
                base64 = Base64.getEncoder().encodeToString(bytes);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return base64;
        }
    
    //資料檔顯示前端
    protected String doUploadFileJsp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	InputStream in = null;
    	FileOutputStream out = null;
        Part file = request.getPart("filepath");
       System.out.println("======"+file.getSubmittedFileName());
        try {
        	in = file.getInputStream();
        	String extString =  file.getSubmittedFileName().substring(file.getSubmittedFileName().lastIndexOf("."));
        	
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    
    //資料檔寫入db
    protected String doUploadFileDb(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InputStream in = null;
		String base64 = null;
        Part file = request.getPart("filepath");
        try {
        	in = file.getInputStream();
            byte[] bytes = IOUtils.toByteArray(in);
            
            base64 = Base64.getEncoder().encodeToString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return base64;
    }
    
    //落地
//	protected void doUploadPic(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//
//		String savePath = "C:\\test";
//		Part part = request.getPart("filepath");
//
//		String header = part.getHeader("Content-Disposition");
//		int start = header.lastIndexOf("=");
//		String fileName = header.substring(start + 1).replace("\"", "");
//
//		if (fileName != null && !"".equals(fileName)) {
//			part.write(savePath + "/" + fileName);
//		}
//
//	}
}
