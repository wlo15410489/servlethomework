package servletEX.servlet;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servletEX.entity.LoginUser;
import servletEX.service.LoginService;
import servletEX.service.impl.LoginServiceImpl;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(value = {"/LoginServlet", "/index", "/main"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getRequestURI().substring(request.getContextPath().length());
		
		if("/LoginServlet".equals(url)) {
			request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}else if("/index".equals(url)){
			doLoginValid(request, response);
		}else if("/main".equals(url)){
			request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
		}
	}
    
    protected void doLoginValid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	LoginService loginService = new LoginServiceImpl();
    	LoginUser loginUser = new LoginUser();
    	loginUser.setUserAccount(request.getParameter("userAccount"));
    	loginUser.setUserPassword(request.getParameter("userPassword"));
    	if(loginService.login(loginUser)) {
    		System.out.println("index");
    		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    	}else {
    		System.out.println("stay");
    		request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    	}
	}
}
