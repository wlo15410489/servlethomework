package servletEX.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import servletEX.dbConnection.ConnectionUtils;
import servletEX.entity.LoginUser;

public class CrudDao {

	public List<Map<String, String>> queryUser(LoginUser loginUser) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		try {
			int index = 0;
			con = ConnectionUtils.getConnection();
			StringBuilder sql = new StringBuilder();

			sql.append("  SELECT ");
			sql.append("    *	 ");
			sql.append("  FROM	 ");
			sql.append(" LOGIN_USER ");
			sql.append(" WHERE	");
			sql.append("     1=1  ");
			
			if (!loginUser.getUserAccount().isEmpty()) {
				sql.append(" AND USER_ACCOUNT = ?  ");
			}
			
			ps = con.prepareStatement(sql.toString());
			
			if (!loginUser.getUserAccount().isEmpty()) {
				index++;
				ps.setString(index, loginUser.getUserAccount());
			}
			
			rs = ps.executeQuery();
			
			while (rs.next()) { 
				Map<String, String> map = new HashMap<String, String>();
				map.put("Id", rs.getString("USER_Id"));
				map.put("Account", rs.getString("USER_ACCOUNT")); 
				map.put("Password", rs.getString("USER_PASSWORD")); 
				map.put("Name", rs.getString("USER_NAME")); 
				map.put("Sex", rs.getString("USER_SEX")); 
				map.put("Phone", rs.getString("USER_PHONE")); 
				list.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null)
				ps.close();
			if (con != null)
				con.close();
		}
		return list;
	}
	
	public void deleteUser(LoginUser loginUser) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = ConnectionUtils.getConnection();
			StringBuilder sql = new StringBuilder();

			sql.append("  DELETE ");
			sql.append("   FROM	 ");
			sql.append(" LOGIN_USER ");
			sql.append(" WHERE	");
			sql.append("     USER_ACCOUNT = ?  ");
			sql.append(" AND USER_PASSWORD = ?  ");
			
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, loginUser.getUserAccount());
			ps.setString(2, loginUser.getUserPassword());
			
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null)
				ps.close();
			if (con != null)
				con.close();
		}
	}
	
	public void insertUser(LoginUser loginUser) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		System.out.println(loginUser.getUserAccount());
		System.out.println(loginUser.getUserPassword());
		System.out.println(loginUser.getUserName());
		System.out.println(loginUser.getUserSex());
		System.out.println(loginUser.getUserPhone());
		try {
			con = ConnectionUtils.getConnection();
			StringBuilder sql = new StringBuilder();

			sql.append("    INSERT         ");
			sql.append("    INTO           ");
			sql.append("    LOGIN_USER     ");
			sql.append("    (              ");
			sql.append("     USER_ACCOUNT  ");
			sql.append("   , USER_PASSWORD ");
			sql.append("   , USER_NAME     ");
			sql.append("   , USER_SEX      ");
			sql.append("   , USER_PHONE    ");
			sql.append("    )     ");
			sql.append("    VALUES");
			sql.append("    (     ");
			sql.append("        ? ");
			sql.append("      , ? ");
			sql.append("      , ? ");
			sql.append("      , ? ");
			sql.append("      , ? ");
			sql.append("    )     ");
			
			ps = con.prepareStatement(sql.toString());
			System.out.println("1111" + sql.toString());
			ps.setString(1, loginUser.getUserAccount());
			ps.setString(2, loginUser.getUserPassword());
			ps.setString(3, loginUser.getUserName());
			ps.setString(4, loginUser.getUserSex());
			ps.setString(5, loginUser.getUserPhone());
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null)
				ps.close();
			if (con != null)
				con.close();
		}
	}
	
	public void updateUser(LoginUser loginUser) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			int index = 0;
			con = ConnectionUtils.getConnection();
			StringBuilder sql = new StringBuilder();

			sql.append("    UPDATE         ");
			sql.append("    LOGIN_USER           ");
			sql.append("    SET     ");
			sql.append("     USER_ACCOUNT = ?  ");
			sql.append("   , USER_PASSWORD = ? ");
			sql.append("   , USER_NAME = ?     ");
			sql.append("   , USER_SEX = ?      ");
			sql.append("   , USER_PHONE = ?    ");
			sql.append("      WHERE  ");
			sql.append("     USER_ID = ? ");
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, loginUser.getUserAccount());
			ps.setString(2, loginUser.getUserPassword());
			ps.setString(3, loginUser.getUserName());
			ps.setString(4, loginUser.getUserSex());
			ps.setString(5, loginUser.getUserPhone());
			ps.setString(6, loginUser.getUserId());
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null)
				ps.close();
			if (con != null)
				con.close();
		}
	}
}
