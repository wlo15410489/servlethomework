package servletEX.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import servletEX.dbConnection.ConnectionUtils;
import servletEX.entity.LoginUser;


public class LoginUserDao {

	public boolean checkUser(LoginUser loginUser) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = ConnectionUtils.getConnection();
			StringBuilder sql = new StringBuilder();

			sql.append("  SELECT ");
			sql.append("    *	 ");
			sql.append("  FROM	 ");
			sql.append(" LOGIN_USER ");
			sql.append(" WHERE	");
			sql.append(" USER_ACCOUNT = ? ");
			sql.append(" AND  ");
			sql.append(" USER_PASSWORD = ? ");

			ps = con.prepareStatement(sql.toString());
			ps.setString(1, loginUser.getUserAccount());
			ps.setString(2, loginUser.getUserPassword());
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null)
				ps.close();
			if (con != null)
				con.close();
		}
		return false;
	}
}
